package com.noticias.lector.service;

import com.noticias.lector.model.Noticia;
import com.noticias.lector.repository.INoticiasRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NoticiasServiceTest {

    @Mock
    private INoticiasRepo noticiasRepo;

    @InjectMocks
    private NoticiasServiceImpl noticiasService;

    @Test
    public void testRetriveAllNoticias(){
        when(noticiasRepo.findAll()).thenReturn(new ArrayList<>());
        assertThat(noticiasService.getNoticias()).isEmpty();

        when(noticiasRepo.findAll()).thenReturn(Collections.singletonList(Noticia.builder()
                .Id(1l)
                .Entradilla("Prueba")
                .Titulo("Probando nuestro service")
                .build()));

        assertThat(noticiasService.getNoticias().size()).isEqualTo(1);
    }

    @Test
    public void testRetriveNoticiaByid() {
        when(noticiasRepo.findById(1l)).thenReturn(Optional.of(Noticia.builder()
                .Id(1l)
                .Entradilla("Prueba")
                .Titulo("Probando nuestro service")
                .build()));

        when(noticiasRepo.findById(2L)).thenReturn(Optional.empty());

        assertThat(noticiasService.getNoticiaById(1L)).isPresent();
        assertThat(noticiasService.getNoticiaById(1L)).get().isEqualTo(Noticia.builder()
                .Id(1l)
                .Entradilla("Prueba")
                .Titulo("Probando nuestro service")
                .build());
        assertThat(noticiasService.getNoticiaById(2L)).isEmpty();

    }
}
