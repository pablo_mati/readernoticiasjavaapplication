package com.noticias.lector.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.noticias.lector.model.Noticia;

@Repository
public interface INoticiasRepo extends JpaRepository<Noticia, Long>{

	
}
