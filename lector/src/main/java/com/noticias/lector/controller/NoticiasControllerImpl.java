package com.noticias.lector.controller;

import com.noticias.lector.LectorApplication;
import com.noticias.lector.model.Noticia;
import com.noticias.lector.service.INoticiasService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "/lector" ,  produces = MediaType.APPLICATION_JSON_VALUE)
public class NoticiasControllerImpl implements INoticiasController{
	
	private static Logger LOG = LoggerFactory.getLogger(LectorApplication.class);
	
	@Autowired
	private INoticiasService iNoticiasService;
	
	@GetMapping("/noticias")
	public ResponseEntity<?> getNoticias(){
		return ResponseEntity.ok(iNoticiasService.getNoticias());
	}
	
	@GetMapping("/noticias/{id}")
    public ResponseEntity<?> getNoticiaById(@PathVariable(value = "id") Long id) {
		return iNoticiasService.getNoticiaById(id).map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }
	
	@PutMapping("/noticias/{id}")
	public ResponseEntity<?> updateOrSaveNoticia(@PathVariable(value = "id") Long id, @RequestBody Noticia noticia){
		
		return ResponseEntity.ok(iNoticiasService.saveOrUpdateNoticia(id, noticia));
	}
	
	@DeleteMapping("/noticias/{id}")
	public ResponseEntity<?> deleteNoticia(@PathVariable("id") Long id){
		iNoticiasService.deleteNoticia(id);
		return ResponseEntity.accepted().build();
	}
	
	

}
