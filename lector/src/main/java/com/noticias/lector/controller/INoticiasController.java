package com.noticias.lector.controller;

import com.noticias.lector.model.Noticia;
import org.springframework.http.ResponseEntity;

public interface INoticiasController {

    ResponseEntity getNoticias();

    ResponseEntity getNoticiaById(Long id);

    ResponseEntity updateOrSaveNoticia(Long id, Noticia noticia);

    ResponseEntity deleteNoticia(Long id);



}
