package com.noticias.lector.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.*;


@Value
@Builder
@RequiredArgsConstructor
@Entity
@Table(name="noticia")
public class Noticia {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long Id;
	
	@Column(name = "titulo", length = 50)
	String Titulo;
	
	@Temporal(TemporalType.TIMESTAMP)
	Date fecha;
	
	@Column(name = "entradilla", length = 200)
	String Entradilla;
	
	@Column(name = "contenido", length = 500)
	String Contenido;


	
}
