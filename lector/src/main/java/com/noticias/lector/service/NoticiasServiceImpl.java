package com.noticias.lector.service;

import com.noticias.lector.LectorApplication;
import com.noticias.lector.exception.NoticiaNotFoundException;
import com.noticias.lector.model.Noticia;
import com.noticias.lector.repository.INoticiasRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class NoticiasServiceImpl implements INoticiasService{
	
	private static Logger LOG = LoggerFactory.getLogger(LectorApplication.class);

	@Autowired
	private INoticiasRepo iNoticiasRepo;
	
	@Override
	public List<Noticia> getNoticias() {
		return iNoticiasRepo.findAll();
	}

	@Override
	public Optional<Noticia> getNoticiaById(Long id) throws NoticiaNotFoundException {
		
		return iNoticiasRepo.findById(id);
		
	}


	@Override
	public Noticia saveOrUpdateNoticia(Long id, Noticia noticia) {

		return iNoticiasRepo.findById(id)
				.map(x -> {
					Noticia.builder()
							.Contenido(noticia.getContenido())
							.Entradilla(noticia.getEntradilla())
							.fecha(noticia.getFecha())
							.Titulo(noticia.getTitulo())
							.build();
					return iNoticiasRepo.save(x);
				})
				.orElseGet(() -> iNoticiasRepo.save(noticia));
	}

	@Override
	public void deleteNoticia(Long id) throws NoticiaNotFoundException {

		iNoticiasRepo.findById(id)
		.map(x -> {
			iNoticiasRepo.delete(x);
			return "Noticia con id: " + id +" ha sido eliminada";})
		.orElseThrow(() -> new NoticiaNotFoundException(id));
	}

}
