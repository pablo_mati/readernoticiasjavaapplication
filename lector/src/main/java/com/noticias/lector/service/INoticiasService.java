package com.noticias.lector.service;

import com.noticias.lector.model.Noticia;

import java.util.List;
import java.util.Optional;

public interface INoticiasService {
	
	List<Noticia> getNoticias();
	
	Optional<Noticia> getNoticiaById(Long id);
	
	Noticia saveOrUpdateNoticia(Long id, Noticia noticia);
	
	void deleteNoticia(Long id);

}
