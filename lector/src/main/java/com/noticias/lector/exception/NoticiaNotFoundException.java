package com.noticias.lector.exception;

public class NoticiaNotFoundException extends RuntimeException {

    public NoticiaNotFoundException(Long id) {

        super(String.format("Notice with Id %d not found", id));
    }
}

